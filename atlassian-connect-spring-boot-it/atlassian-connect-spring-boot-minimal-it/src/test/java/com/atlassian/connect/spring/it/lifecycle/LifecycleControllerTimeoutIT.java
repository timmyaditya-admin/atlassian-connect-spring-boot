package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AsymmetricKeys;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.nimbusds.jose.JOSEException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleEventMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyIterable.emptyIterable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LifecycleControllerTimeoutIT extends BaseApplicationIT {

    private static final Duration INSTALL_DURATION = Duration.of(4, ChronoUnit.SECONDS);

    private static final Duration CLIENT_TIMEOUT = Duration.of(10, ChronoUnit.SECONDS);

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    private MockRestServiceServer mockServer;

    private String appBaseUrl;

    private static final URI PRODUCTION_INSTALL_KEYS_URL = URI.create("https://example.keys.com/");

    @Before
    public void before() throws JOSEException {
        AsymmetricKeys.generateKeys(UUID.randomUUID().toString());
        properties.setPublicKeyBaseUrl(PRODUCTION_INSTALL_KEYS_URL.toString());
        RestTemplate restTemplate = asymmetricPublicKeyProvider.getAsymmetricPublicKeyProviderRestTemplate();
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + AsymmetricKeys.getKeyId())).andRespond(withSuccess(AsymmetricKeys.getPublicKey(), MediaType.TEXT_PLAIN));
        appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
    }

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Test
    public void shouldReturnServiceUnavailableOnServerTimeout() throws Exception {
        doAnswer(getInstallTimeoutAnswer()).when(hostRepository).findById(any(String.class));
        AtlassianHost host = new AtlassianHostBuilder().build();
        TestRestTemplate restTemplate = createRestTemplateWithTimeout(host);
        ResponseEntity<String> response = restTemplate.postForEntity(URI.create(getServerAddress() + "/installed"),
                createLifecycleEventMap("installed"), String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.SERVICE_UNAVAILABLE));

        waitForInstallToFinish();
        assertThat(hostRepository.findAll(), is(emptyIterable()));
    }

    private TestRestTemplate createRestTemplateWithTimeout(AtlassianHost host) throws Exception {
        ClientHttpRequestFactory requestFactory = createRequestFactoryWithTimeout();
        restTemplateBuilder.requestFactory(() -> requestFactory).errorHandler(new NoopErrorHandler());
        return new SimpleJwtSigningRestTemplate(restTemplateBuilder, host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
    }

    private HttpComponentsClientHttpRequestFactory createRequestFactoryWithTimeout() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        int timeout = Math.toIntExact(CLIENT_TIMEOUT.toMillis());
        httpRequestFactory.setConnectionRequestTimeout(timeout);
        httpRequestFactory.setConnectTimeout(timeout);
        httpRequestFactory.setReadTimeout(timeout);
        return httpRequestFactory;
    }

    private Answer<Optional<AtlassianHost>> getInstallTimeoutAnswer() {
        return invocation -> {
            try {
                Thread.sleep(INSTALL_DURATION.toMillis());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return Optional.empty();
        };
    }

    private void waitForInstallToFinish() throws InterruptedException {
        Thread.sleep(INSTALL_DURATION.plus(1, ChronoUnit.SECONDS).toMillis());
    }

    /**
     * Workaround for https://github.com/spring-projects/spring-boot/issues/7033.
     */
    @TestConfiguration
    public static class MockRepositoryConfiguration {

        @Primary
        @Bean("atlassianHostRepositoryMock")
        public AtlassianHostRepository atlassianHostRepository(final AtlassianHostRepository hostRepository) {
            return mock(AtlassianHostRepository.class, AdditionalAnswers.delegatesTo(hostRepository));
        }
    }

    private static class NoopErrorHandler implements ResponseErrorHandler {

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            return false;
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {}
    }
}
