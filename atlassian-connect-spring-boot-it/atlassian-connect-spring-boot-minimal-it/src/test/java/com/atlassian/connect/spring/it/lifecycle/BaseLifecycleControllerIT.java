package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.security.PrivateKey;

import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public abstract class BaseLifecycleControllerIT extends BaseApplicationIT {

    public MockHttpServletRequestBuilder postInstalled(String path, String secret) throws Exception {
        return postLifecycleCallback(path, "installed", secret);
    }

    public MockHttpServletRequestBuilder postUninstalled(String path, String secret) throws Exception {
        return postLifecycleCallback(path, "uninstalled", secret);
    }

    private MockHttpServletRequestBuilder postLifecycleCallback(String path, String eventType, String secret) throws JsonProcessingException {
        final String lifecycleJson = createLifecycleJson(eventType, secret);
        return postLifecycleCallbackEvent(path, lifecycleJson);
    }

    private MockHttpServletRequestBuilder postLifecycleCallbackEvent(String path, String lifecycleJson) {
        return post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(lifecycleJson);
    }
}
