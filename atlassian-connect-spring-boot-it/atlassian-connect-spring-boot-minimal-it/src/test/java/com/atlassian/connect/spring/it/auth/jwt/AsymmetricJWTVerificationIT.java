package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AsymmetricKeys;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LifecycleBodyHelper;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nimbusds.jose.JOSEException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AsymmetricJWTVerificationIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    private MockRestServiceServer mockServer;


    private static final URI PRODUCTION_INSTALL_KEYS_URL = URI.create("https://example.keys.com/");

    @Before
    public void before() throws JOSEException {
        AsymmetricKeys.generateKeys(UUID.randomUUID().toString());
        properties.setPublicKeyBaseUrl(PRODUCTION_INSTALL_KEYS_URL.toString());
        hostRepository.deleteAll();
        RestTemplate restTemplate = asymmetricPublicKeyProvider.getAsymmetricPublicKeyProviderRestTemplate();
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + AsymmetricKeys.getKeyId())).andRespond(withSuccess(AsymmetricKeys.getPublicKey(), MediaType.TEXT_PLAIN));
    }

    @Test
    public void shouldCreateHostFromInstalledEvent() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertEquals(this.hostRepository.count(), 1);
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
    }

    @Test
    public void shouldParsePublicKeyWithDifferentLineBreakFormats() throws Exception {
        mockServer.reset();
        String publicKeyWin = AsymmetricKeys.getPublicKey().replace(System.lineSeparator(), "\r\n");
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + AsymmetricKeys.getKeyId())).andRespond(withSuccess(publicKeyWin, MediaType.TEXT_PLAIN));

        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertEquals(this.hostRepository.count(), 1);
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
    }

    @Test
    public void shouldUseAsymmetricAuthenticationOnSecondInstall() throws JOSEException, JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertEquals(this.hostRepository.count(), 1);
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
    }

    @Test
    public void shouldUninstall() throws JOSEException, JsonProcessingException {

        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        host = hostRepository.save(host);

        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String uninstalledJson = LifecycleBodyHelper.createLifecycleJson("uninstalled", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(uninstalledJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getUninstalledURI(), request, String.class);

        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(hostRepository.count(), is(1L));
        AtlassianHost updated = hostRepository.findAll().iterator().next();
        assertEquals("Addon should not be installed", updated.isAddonInstalled(), false);
    }

    @Test
    public void shouldRejectIfSignatureIsInvalidOnUninstall() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());

        AsymmetricKeys.generateKeys(AsymmetricKeys.getKeyId());
        mockServer.reset();
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + AsymmetricKeys.getKeyId())).andRespond(withSuccess(AsymmetricKeys.getPublicKey(), MediaType.TEXT_PLAIN));

        String uninstalledJson = LifecycleBodyHelper.createLifecycleJson("uninstalled", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(uninstalledJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getUninstalledURI(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(this.hostRepository.count(), 0);
    }

    @Test
    public void shouldRejectSecondUnsignedInstall() throws JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new TestRestTemplate();
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldRejectIfAudienceDoesNotMatchBaseUrl() throws JsonProcessingException, JOSEException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String differentAppBaseUrl = "https://example.com";
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, differentAppBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());

        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(this.hostRepository.count(), 0);
    }

    @Test
    public void shouldRejectIfSignatureIsInvalid() throws JsonProcessingException, JOSEException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());

        AsymmetricKeys.generateKeys(AsymmetricKeys.getKeyId());
        mockServer.reset();
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + AsymmetricKeys.getKeyId())).andRespond(withSuccess(AsymmetricKeys.getPublicKey(), MediaType.TEXT_PLAIN));

        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(this.hostRepository.count(), 0);
    }

    @Test
    public void shouldFailIfKeyIdIsInvalid() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        String ramdonKeyId = UUID.randomUUID().toString();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), ramdonKeyId);
        mockServer.reset();
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + ramdonKeyId))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));

        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(this.hostRepository.count(), 0);
    }

    @Test
    public void shouldNotFallBackToSymmetricSignaturesOnFirstInstall() throws JOSEException, JsonProcessingException {
        String secret = "secret";
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        TestRestTemplate unsigned = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = unsigned.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(0, this.hostRepository.count());
    }

    @Test
    public void shouldNotFallBackToSymmetricSignaturesOnSecondInstallWithFallbackDisabled() throws JOSEException, JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertEquals(1, this.hostRepository.count());
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertEquals(insertedHost.getClientKey(), host.getClientKey());
    }

    private URI getInstalledUri() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/installed").build().toUri();
    }

    private URI getUninstalledURI() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/uninstalled").build().toUri();
    }

}
