package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import org.springframework.http.HttpRequest;

import java.util.Optional;

public class OAuth2HttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private AtlassianHost host;

    public OAuth2HttpRequestInterceptor(AtlassianHost host, String atlassianConnectClientVersion) {
        super(atlassianConnectClientVersion);
        this.host = host;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        assertRequestToHost(request, host);
        return Optional.of(host);
    }
}
