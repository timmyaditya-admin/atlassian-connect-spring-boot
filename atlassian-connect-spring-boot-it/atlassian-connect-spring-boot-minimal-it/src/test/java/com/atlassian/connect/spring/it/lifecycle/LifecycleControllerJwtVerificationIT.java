package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AsymmetricJWTBuilder;
import com.atlassian.connect.spring.it.util.AsymmetricKeys;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nimbusds.jose.JOSEException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LifecycleControllerJwtVerificationIT extends BaseApplicationIT {
    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    private MockRestServiceServer mockServer;

    private String appBaseUrl;

    private static final URI PRODUCTION_INSTALL_KEYS_URL = URI.create("https://example.keys.com/");

    @Before
    public void before() throws JOSEException {
        AsymmetricKeys.generateKeys(UUID.randomUUID().toString());
        properties.setPublicKeyBaseUrl(PRODUCTION_INSTALL_KEYS_URL.toString());
        RestTemplate restTemplate = asymmetricPublicKeyProvider.getAsymmetricPublicKeyProviderRestTemplate();
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + AsymmetricKeys.getKeyId())).andRespond(withSuccess(AsymmetricKeys.getPublicKey(), MediaType.TEXT_PLAIN));
        appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
    }

    @Test
    public void shouldAcceptInstallForJwtWithQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        TestRestTemplate restTemplate =  SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        ResponseEntity<Void> response = postLifecycle(restTemplate, "installed");
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void shouldRejectInstallForJwtWithoutQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String jwt = new AsymmetricJWTBuilder().issuer(host.getClientKey()).audience(appBaseUrl)
                .asymmetricSignature(AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId()).build();
        ResponseEntity<Void> response = postLifecycle(new SimpleJwtSigningRestTemplate(jwt), "installed");
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void shouldAcceptUninstallForJwtWithQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        TestRestTemplate restTemplate =  SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        ResponseEntity<Void> response = postLifecycle(restTemplate, "uninstalled");
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    @Test
    public void shouldRejectUninstallForJwtWithoutQsh() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String jwt = new AsymmetricJWTBuilder().issuer(host.getClientKey()).audience(appBaseUrl)
                .asymmetricSignature(AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId()).build();
        ResponseEntity<Void> response = postLifecycle(new SimpleJwtSigningRestTemplate(jwt), "uninstalled");
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    private ResponseEntity<Void> postLifecycle(TestRestTemplate restTemplate, String eventType) throws JsonProcessingException {
        return restTemplate.exchange(RequestEntity.post(getRequestUri(eventType))
                .contentType(MediaType.APPLICATION_JSON)
                .body(createLifecycleJson(eventType, SHARED_SECRET)), Void.class);
    }

    private URI getRequestUri(String path) {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path(path).build().toUri();
    }
}
