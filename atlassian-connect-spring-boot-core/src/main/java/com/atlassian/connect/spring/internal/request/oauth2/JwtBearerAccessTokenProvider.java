package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.resource.UserApprovalRequiredException;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.security.oauth2.client.token.AccessTokenProvider;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Calendar;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

public class JwtBearerAccessTokenProvider implements AccessTokenProvider {

    public static final ParameterizedTypeReference<Map<String, String>> ACCESS_TOKEN_RESPONSE_TYPE = new ParameterizedTypeReference<Map<String, String>>() {};

    private static final Logger log = LoggerFactory.getLogger(JwtBearerAccessTokenProvider.class);

    private static final int CLOCK_SKEW_SECONDS = 60;

    private final RestTemplate restTemplate;

    private final AtlassianHostUser hostUser;

    private final URI authorizationServerUrl;

    private OAuth2JwtAssertionGenerator jwtAssertionGenerator;

    public JwtBearerAccessTokenProvider(AtlassianHostUser hostUser, URI authorizationServerUrl,
            RestTemplate restTemplate, OAuth2JwtAssertionGenerator jwtAssertionGenerator) {
        this.hostUser = hostUser;
        this.restTemplate = restTemplate;
        this.authorizationServerUrl = authorizationServerUrl;
        this.jwtAssertionGenerator = jwtAssertionGenerator;
    }

    @Override
    public OAuth2AccessToken obtainAccessToken(OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails,
                                               AccessTokenRequest accessTokenRequest)
            throws UserRedirectRequiredException, UserApprovalRequiredException, AccessDeniedException {

        log.debug("Requesting OAuth 2.0 access token using {}", oAuth2ProtectedResourceDetails.getGrantType());
        String assertionString = jwtAssertionGenerator.getAssertionString(hostUser, authorizationServerUrl);

        final HttpHeaders contentTypeHeader = new HttpHeaders();
        contentTypeHeader.setContentType(APPLICATION_FORM_URLENCODED);
        final HttpEntity<MultiValueMap<String, String>> accessTokenRequestEntity =
                new HttpEntity<>(accessTokenRequestParameters(assertionString,
                        oAuth2ProtectedResourceDetails.getGrantType()), contentTypeHeader);

        final ResponseEntity<Map<String, String>> accessTokenResponse = restTemplate.exchange(
                oAuth2ProtectedResourceDetails.getAccessTokenUri(), HttpMethod.POST,
                accessTokenRequestEntity, ACCESS_TOKEN_RESPONSE_TYPE);

        OAuth2AccessToken accessToken = DefaultOAuth2AccessToken.valueOf(accessTokenResponse.getBody());
        if (accessToken instanceof DefaultOAuth2AccessToken) {
            reduceExpirationTime((DefaultOAuth2AccessToken) accessToken, CLOCK_SKEW_SECONDS);
        }
        return accessToken;
    }

    @Override
    public boolean supportsResource(OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails) {
        return oAuth2ProtectedResourceDetails instanceof JwtBearerResourceDetails;
    }

    @Override
    public OAuth2AccessToken refreshAccessToken(OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails,
                                                OAuth2RefreshToken oAuth2RefreshToken, AccessTokenRequest accessTokenRequest)
            throws UserRedirectRequiredException {
        return obtainAccessToken(oAuth2ProtectedResourceDetails, accessTokenRequest);
    }

    @Override
    public boolean supportsRefresh(OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails) {
        return false;
    }

    private static LinkedMultiValueMap<String, String> accessTokenRequestParameters(String assertion, String grantType) {
        LinkedMultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("grant_type", grantType);
        parameters.add("assertion", assertion);
        return parameters;
    }

    private void reduceExpirationTime(DefaultOAuth2AccessToken accessToken, int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(accessToken.getExpiration());
        calendar.add(Calendar.SECOND, -seconds);
        accessToken.setExpiration(calendar.getTime());
    }
}
