package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthentication;
import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class AsymmetricAuthentication extends JwtAuthentication {

    public static final String ROLE_ASYMMETRIC_JWT = "ROLE_ASYMMETRIC_JWT";

    public AsymmetricAuthentication(JWTClaimsSet claims, AtlassianHostUser hostUser, String queryStringHash) {
        super(hostUser, claims, queryStringHash);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(ROLE_ASYMMETRIC_JWT));
    }

}
