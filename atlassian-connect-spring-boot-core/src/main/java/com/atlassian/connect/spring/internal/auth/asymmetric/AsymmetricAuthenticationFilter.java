package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import com.atlassian.connect.spring.internal.auth.jwt.InvalidAsymmetricJwtException;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


public class AsymmetricAuthenticationFilter extends OncePerRequestFilter {
    private static final String AUTHORIZATION_HEADER_SCHEME_PREFIX = "JWT ";
    private static final Logger log = LoggerFactory.getLogger(AsymmetricAuthenticationFilter.class);
    private final LifecycleURLHelper lifecycleURLHelper;
    private AuthenticationManager authenticationManager;
    private AddonDescriptorLoader addonDescriptorLoader;
    private AuthenticationFailureHandler failureHandler;
    private AtlassianConnectProperties atlassianConnectProperties;

    public AsymmetricAuthenticationFilter(AuthenticationManager authenticationManager,
                                          AddonDescriptorLoader addonDescriptorLoader,
                                          ServerProperties serverProperties,
                                          LifecycleURLHelper lifecycleURLHelper,
                                          AtlassianConnectProperties atlassianConnectProperties) {
        this.authenticationManager = authenticationManager;
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.failureHandler = createFailureHandler(serverProperties);
        this.lifecycleURLHelper = lifecycleURLHelper;
        this.atlassianConnectProperties = atlassianConnectProperties;
    }

    private static Optional<String> getJwtFromHeader(HttpServletRequest request) {
        Optional<String> optionalJwt = Optional.empty();
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!StringUtils.isEmpty(authHeader) && authHeader.startsWith(AUTHORIZATION_HEADER_SCHEME_PREFIX)) {
            String jwt = authHeader.substring(AUTHORIZATION_HEADER_SCHEME_PREFIX.length());
            optionalJwt = Optional.of(jwt);
        }

        return optionalJwt;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Optional<String> optionalJwt = getJwtFromHeader(request);

        if (!lifecycleURLHelper.isRequestToLifecycleURL(request)) {
            log.debug("Ignoring non-lifecycle endpoint");
            filterChain.doFilter(request, response);
            return;
        }

        if (!optionalJwt.isPresent()) {
            String message = "Failed to authenticate install/uninstall request due to missing JWT token.";
            log.error(message);
            failureHandler.onAuthenticationFailure(request, response, new InvalidAsymmetricJwtException(message));
            return;
        }

        Authentication authenticationRequest = createPKIAuthenticationToken(request, optionalJwt.get());
        Authentication authenticationResult;

        try {
            authenticationResult = authenticationManager.authenticate(authenticationRequest);
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
        } catch (ProviderNotFoundException e) {
            String message = "Failed to authenticate install/uninstall lifecycle request.";
            log.error(message, e);
            failureHandler.onAuthenticationFailure(request, response, new InvalidAsymmetricJwtException(message));
            return;
        }

        filterChain.doFilter(request, response);
    }

    private SimpleUrlAuthenticationFailureHandler createFailureHandler(ServerProperties serverProperties) {
        SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler(serverProperties.getError().getPath());
        failureHandler.setAllowSessionCreation(false);
        failureHandler.setUseForward(true);
        return failureHandler;
    }

    private AsymmetricAuthenticationToken createPKIAuthenticationToken(HttpServletRequest request, String jwt) {
        log.debug("Retrieved JWT from request");
        CanonicalHttpServletRequest canonicalHttpServletRequest = new CanonicalHttpServletRequest(request);
        AsymmetricCredentials credentials = new AsymmetricCredentials(jwt, canonicalHttpServletRequest);
        return new AsymmetricAuthenticationToken(credentials);
    }
}
